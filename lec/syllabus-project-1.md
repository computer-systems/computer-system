
# Table of Contents

1.  [Description](#orgc5c7bb8)
2.  [Systems and Complexity of Systems (1)](#org8cea7ba)
    1.  [Complex computer systems](#org1c151b5)
    2.  [Complexity](#org1fd7904)
    3.  [Why complex](#org24c3f44)
    4.  [Ways of fighting complexity](#org6bdaaef)
    5.  [Hardware and software parts](#org513dcd7)
        1.  [Origins of distinction](#org3f31497)
        2.  [Software is harder](#orgc36de54)
        3.  [Modularization in hardware](#orgcf5ad45)
        4.  [Layers](#org6e3c5d5)
        5.  [Why it does not matter](#org7a8f015)
    6.  [Indirection helps connecting modules](#orgf6f567e)
    7.  [Three types of functional components](#org1f9865c)
3.  [Component study: Memory](#org3f0b9b2)
    1.  [Interface](#orge865012)
    2.  [Examples](#org1396083)
    3.  [Properties](#org661eb70)
        1.  [Read/Write Coherence  (1)](#orgbcc3778)
        2.  [Atomicity](#org7ae7191)
        3.  [Latency](#org1be9500)
        4.  [Addressing data](#orge9b632f)
4.  [Component study: Transport](#org6963357)
    1.  [Interface](#org657dd05)
    2.  [Examples](#orga60b616)
    3.  [Challenges of physical networks](#org61a85cb)
        1.  [Physics](#orgb50a86e)
        2.  [Sharing](#org2dde548)
    4.  [Particularities of network exchange](#orga6907a6)
        1.  [Isochronous vs asynchronous multiplexing](#orgc5af00b)
        2.  [Packet queues](#org2e6acb5)
        3.  [Best-effort networks](#orgb023732)
        4.  [Sending multisegmented messages](#org7f6fe15)
        5.  [Organization in layers](#org7240843)
    5.  [Leaking abstractions](#org287884a)
5.  [Component: Executor](#org313daba)
    1.  [Examples](#orgd1f0ac4)
    2.  [Description](#org9ce9811)
        1.  [Informal description](#org1ee3b43)
        2.  [Formal description](#org0a2acfd)
    3.  [Layering](#org0c85f7c)
    4.  [Models of computations and programming styles](#orgc9c6371)
        1.  [Based on von Neumann architecture](#orgc3be3c7)
        2.  [Based on rewriting](#org97c4eab)
        3.  [Based on logics](#orgf26b96a)
        4.  [Promoting exploration](#orgbcfaf8b)
        5.  [Specialized processors, DSP](#orgea90d11)
6.  [Systemic properties](#org34508bf)
    1.  [Virtualization](#org8e192bf)
        1.  [Examples](#orgbfa1fe0)
        2.  [Goals](#orgd833d29)
        3.  [Kinds of virtualization](#org978744e)
    2.  [Performance](#org125ccfa)
        1.  [What impedes performance](#orga6f7d59)
        2.  [Latency and throughput](#org44294b3)
        3.  [Parallelism and concurrency](#org64e1aeb)
        4.  [GPU architecture](#orga8df5e2)
        5.  [actor model](#orga036b92)
    3.  [Errors](#org50060eb)
        1.  [Basic steps](#org2922469)
        2.  [Faults, Failures, Errors](#org4f42a40)
7.  [Notes](#org85ffd8a)



<a id="orgc5c7bb8"></a>

# Description

**Aspects**

-   Classifying types of functional components; studying their properties (independently of implementation).
-   Some typical configurations. CPU internals, Harvard/Princeton architectures, FGPA, GPUs are here.
-   Systemic properties: reliability, performance, modularity

**Key point**

Why systems become more complex? How to build complex systems with desired properties with minimal possible complexity?

**Why this course is important for you**

-   Gives food for thought: "I am making a computer system where data is going to be stored like that,

processed like that, transported like that. What problems to expect and how can they be solved?"

-   Gives recipes on increasing performance and reliability
-   Connects different more specialized courses so you can use them all together.
-   Blurs the border between low- and high-level, software and hardware.
-   Gives a broad overview of technologies and solutions that may fit your specific problem perfectly


<a id="org8cea7ba"></a>

# Systems and Complexity of Systems (1)


<a id="org1c151b5"></a>

## Complex computer systems

-   A system is a set of **interconnected components** that has an expected behavior observed through its **interface**.
-   Through its interface we observe the processes inside and interpret their results &#x2013; these are **computations**.
    -   Examples: Pulsars, `random.org`.
-   Structural and functional decomposition
    -   Examples: scissors, cursor, program receiving IP packets from network, functions,
-   A running program is a system, incorporating hardware and software


<a id="org1fd7904"></a>

## Complexity

Signs of complexity:

-   Many components
-   Many interconnections
-   Many irregularities
-   Long description
-   Huge team is working on it


<a id="org24c3f44"></a>

## Why complex

-   Cascading and interacting requirements
-   Maximizing the resource usage
-   In computer systems many components and interconnections are possible because of:
    -   Error correction
    -   Not strictly limited by laws of physics


<a id="org6bdaaef"></a>

## Ways of fighting complexity

-   Modularity
    -   hardware constructor
        -   libraries
-   Abstraction
    -   functions
    -   classes
    -   API
-   Layering
    -   assembler into C into JVM bytecode into application
-   Hierarchies
    -   distributed *anything*


<a id="org513dcd7"></a>

## Hardware and software parts


<a id="org3f31497"></a>

### Origins of distinction

-   from simple control systems to von Neumann
-   software is easier to fix and modify
-   one machine &#x2013; many tasks


<a id="orgc36de54"></a>

### Software is harder

-   no physical restrictions
-   changes much faster


<a id="orgcf5ad45"></a>

### Modularization in hardware

-   independent scaling
-   specified interfaces


<a id="org6e3c5d5"></a>

### Layers

Different organizations are responsible for different layers:

-   hardware
-   hypervisor
-   OS
-   HAL
-   language virtual machines
-   applications


<a id="org7a8f015"></a>

### Why it does not matter

Functional decomposition is more useful.

1.  Moving computations back to hardware

    -   accelerating graphics
    -   power consumption and computational power, FGPU
        -   e.g. cryptocurrency mining


<a id="orgf6f567e"></a>

## Indirection helps connecting modules

-   Names allow to
    -   postpone decisions
    -   share modules
    -   replace modules
-   Indirection degrees (value, variable, address, function&#x2026; see "elements of clojure")


<a id="org1f9865c"></a>

## Three types of functional components

-   Executor
-   Memory
-   Transport
    
    Talking about both hardware and software components


<a id="org3f0b9b2"></a>

# Component study: Memory

Stores data.


<a id="orge865012"></a>

## Interface

-   `write( location, data )`
-   `value <- read( location )`


<a id="org1396083"></a>

## Examples

-   Registers
-   Hardware caches
-   Physical RAM
-   Virtual memory
-   SSD/HDD
-   RAID
-   File systems, also RAM filesystem
-   Database, registry
-   Distributed filesystem
-   Cloud storage
-   Distributed databases


<a id="org661eb70"></a>

## Properties


<a id="orgbcc3778"></a>

### Read/Write Coherence  (1)

**Coherence**: result of the read is always the same as the most recent write.

-   Concurrency
    -   data races
    -   interrupts
-   Remote storage
    -   cache coherence
-   Performance enhancements
    -   compiler reordering
-   Cell size not equal value size
    -   How this affects Java VM
-   Replication [for performance, reliability]
    -   **[performance]** ccNUMA; Multiple caches of the same shared resource; one of them is
        changed, others become incoherent
    -   **[reliability]** online shop with distributed storage
-   Distribution (no other choice)
    -   Shards


<a id="org7ae7191"></a>

### Atomicity

Atomicity is a form of modularity which makes the implementation of atomic
operation indiscoverable by upper layers of system.

*Before-after* atomicity (completely before/after other actions) and *all-or-nothing* (either completes or does not happen)

1.  All-or-nothing atomicity

    -   Relevant examples
        -   databases (updating several records without breaking invariants)
        -   hardware (handling interrupts)
        -   operating systems (supervisor system call)
        -   everyday programming (faults in lower layers)
    
    -   software transactional memory
    -   atomic reads/writes
    -   journal file system

2.  Before-after atomicity

    *The result of every read or write is as if that read or write occurred
    either completely before or completely after any other read or write.*
    
    -   Examples
        -   file opening/locking
        -   caching
        -   CAS instructions

3.  Memory models: weak and strong

    -   Preshing's classification: from almost no guarantees to sequentially consistent
    -   Links to hardware memory and software memory model (C/C++ standards, Intel and ARM docs, Java VM&#x2026;)
    -   Reorderings (demonstrate compiler and hardware reorderings)
    -   *[[<https://en.wikipedia.org/wiki/Happened-before>][Happened-before* relation]]
    -   Acquire-release semantics

4.  Achieving atomicity systematically

    1.  all-or-nothing atomicity
    
        -   Never change the only copy of data
        -   Shadow copy
        -   Keeping version history
    
    2.  before-after atomicity
    
        -   Serializing
        -   Locking disciplines: simple, two-phase,
        -   Deadlock prevention:
            lock ordering, backing out, timers, cycle detection

5.  Atomicity across layers and sites

    -   Atomicity on higher levels: undoing lower-level operations vs delaying their commitment
    -   Two-phase commit
    -   Multiple-site two-phase commit


<a id="org1be9500"></a>

### Latency

Time to complete a read or write.

1.  How fast are memories and storage

    Comparison chart

2.  Reducing through exploiting workload properties

    -   Locality (temporal and spatial) makes caching work.
    -   Principle of "faster in average, slower in the worst case"

3.  Reducing through concurrency

    -   RAID-like schemes, distributed storage

4.  Reducing through packaging reads and writes

    It is better to read/write chunks of memory.
    
    -   C library: buffering
    -   Databases
    -   Compiler optimizations

5.  Alignment

    -   aligned reads/writes on AMD64

6.  Consecutive vs random access

    -   Show performance test
    -   Depending on underlying data structure different access patterns are fast.
        -   Arrays vs lists vs double linked lists vs trees vs hashmaps

7.  Prefetching

    -   Show performance test
    -   Prefetching from Internet (smart web-browser)

8.  Managing

    -   Fragmentation
        -   in heap
        -   in filesystems
        -   in databases
    -   Garbage collection
    -   Rust and affine types


<a id="orge9b632f"></a>

### Addressing data

-   Naming

1.  Hardware

    -   linear addressing
    -   associative memory
    -   physical vs virtual addresses

2.  Software

    -   file names
    -   identifiers
    -   addresses
        -   different equalities in LISP, C, Java

3.  Case study: layers in UNIX filesystem

    From sectors to blocks, inodes, paths and filenames


<a id="org6963357"></a>

# Component study: Transport


<a id="org657dd05"></a>

## Interface

-   `send( where, what )`
-   `receive( from_where, what )`


<a id="orga60b616"></a>

## Examples

-   hardware bus
-   pipes
-   function calling convention
-   shared memory
-   ip, tcp, udp, http(s), ftp and other protocols of various levels
-   loopback
-   temp files/sockets
-   buffers
-   physical networks: wired and wireless
-   all sorts of hardware interfaces like USB, FireWire, Thunderbolt, HDMI&#x2026;
-   VPN
-   Torrents


<a id="org61a85cb"></a>

## Challenges of physical networks


<a id="orgb50a86e"></a>

### Physics

For hardware-based

-   Range of delays: from 10 nanoseconds (same room) to 300 ms satellite (lower bound by speed of light)
-   Hostile environment
-   Very limited bandwidth
-   Physical sharing


<a id="org2dde548"></a>

### Sharing

-   Routing
-   Economics (putting cables around is expensive and will stay this way) and legislation (very slow to change)

1.  Wide range of parameters

    -   propagation time
    -   data transmission rates
    -   number of nodes in network
    -   very uneven usage


<a id="orga6907a6"></a>

## Particularities of network exchange


<a id="orgc5af00b"></a>

### Isochronous vs asynchronous multiplexing

-   Depending on application characteristics
    -   Continuous stream of data: better iso-, async is a waste,
    -   Bursts: better async, iso- bad for variable latency
    -   Response to load variations: iso- either accepts or blocks, async- more flexible (variable delay, discards data or adapt rates)
-   Asynchronous is more complex (trying to maximize the resource usage)
-   Software case study: synchronous vs asynchronous sockets


<a id="org2e6acb5"></a>

### Packet queues

-   Planning for the worst case
    Allocate buffers big enough to handle the worst case
-   Planning for the usual size and fight back 
    Chose a buffer size; on overflow send messages asking to stop sending
-   Planning for the usual size and discard overflow
    -   Chose a buffer size; on overflow discard packets silently
    -   The sender will adapt the rate
-   *Best-effort* network vs *guaranteed delivery* network


<a id="orgb023732"></a>

### Best-effort networks

-   Variable delays
-   Variable transmission rates
-   Discarded packets
-   Duplicate packets
-   Maximum packet length
-   Reordered delivery


<a id="org7f6fe15"></a>

### Sending multisegmented messages

-   Lockstep
    Very slow and inefficient
-   Overlapping transmissions
    Resemblance to CPU pipeline
-   At-least-once delivery, at-most-once delivery
    -   Adding states to the sender/receiver: which are are already sent/received?
    -   Problem: we can store such information indefinitely


<a id="org7240843"></a>

### Organization in layers

-   Independence/opaqueness of layers
    Each next layer has no information about previous, it works with raw data
-   Three layers: End-to-end  + Best-effort network on network and link layers
-   Three protocols
    -   application
    -   presentation
    -   transport
-   Examples
    -   Internet
    -   calling C function from JVM
    -   pipes
    -   shared memory


<a id="org287884a"></a>

## Leaking abstractions

-   reliable TCP over unreliable IP
-   \`Int\` and \`int\`
-   floating point arithmetic is not real
-   iterating over 2-dimensional array horizontally or vertically
-   *So the abstractions save us time working, but they don’t save us time learning*


<a id="org313daba"></a>

# Component: Executor

Active component.


<a id="orgd1f0ac4"></a>

## Examples

-   Programming languages interpreters
-   Internet browsers
-   CPU
-   Controller
-   Compiler
-   Word processors
-   Games


<a id="org9ce9811"></a>

## Description

It is easier to describe memory or transport.
Very different approaches to describe the semantics of computing.


<a id="org1ee3b43"></a>

### Informal description

Suffers from the natural language's ambiguity.

-   Language standards
-   Documentation on programs


<a id="org0a2acfd"></a>

### Formal description

1.  Three main types

    -   **Operational**
        Precise effect on the computation state; like a machine.
        -   Instruction reference
            Where to find the next instruction? More properly a continuation.
        -   Instruction semantics
        -   Machine state
    -   Axiomatic semantics
        Effect on a set of assertions about program state.
        Assertions are not describing the state in a precise way.
    -   Denotational
        Translate program into another language, e.g. set theoretic

2.  Mixed and unorthodox types

    -   Algebraic semantics
    -   Action semantics (denotational + operational + algebraic)
    -   Concurrency (from operational)
        Describe in terms of parallel concurrent execution
        -   Process calculi, actors, Petri nets
    -   Predicate transformational
        -   Weakest preconditions
    -   Game (from denotational)
    -   Categorial

3.  How using rewriting systems looks like

    Which language is self-explanatory enough? Choice of λ -calculus + logics.
    
    -   state (tuples, maps&#x2026;)
    -   formal language with syntax and semantics


<a id="org0c85f7c"></a>

## Layering

-   Virtual machines
-   Sedov's law


<a id="orgc9c6371"></a>

## Models of computations and programming styles

Mostly case studies


<a id="orgc3be3c7"></a>

### Based on von Neumann architecture

1.  classic von Neumann

    -   What is easy and what is hard?

2.  Princeton/Harvard architectures

    -   Advantages of having separate data and instructions memory
    -   Instruction and data cache

3.  Stack and registry machines

    -   Language virtual machines (JVM, CLR, &#x2026;)

4.  Instruction set formats

    -   CISC/RISC
    -   (S|M)I(S|M)D
    -   VLIW

5.  Convenient styles

    -   structured imperative programming
    -   state machines


<a id="org97c4eab"></a>

### Based on rewriting

1.  λ - calculus and functional programming

    -   ML or F# samples
    -   Algebraic datatypes + recursion
    -   What is easy and what is hard
    -   [Expression problem](https://oleksandrmanzyuk.wordpress.com/2014/06/18/from-object-algebras-to-finally-tagless-interpreters-2/)

2.  Convenient styles

    -   Functional programming
    -   Object-oriented and its flavors

3.  


<a id="orgf26b96a"></a>

### Based on logics

-   SAT/SMT solvers
    -   What's easy to do?
-   Prolog and solution lookup
    -   What's easy to do?


<a id="orgbcfaf8b"></a>

### Promoting exploration

-   Smalltalk
-   LISP
-   Forth


<a id="orgea90d11"></a>

### Specialized processors, DSP


<a id="org34508bf"></a>

# Systemic properties


<a id="org8e192bf"></a>

## Virtualization

We already know about client-service architecture


<a id="orgbfa1fe0"></a>

### Examples

-   Networks
-   Containers
-   Hypervisors
-   Virtual machines like VMWare/Virtualbox
-   Clouds


<a id="orgd833d29"></a>

### Goals

-   Flexibility
-   Enforcing modularity
-   Optimizing resource usage
-   Transparent migration


<a id="org978744e"></a>

### Kinds of virtualization

-   One-to-one
-   Many-to-one
-   One-to-many


<a id="org125ccfa"></a>

## Performance


<a id="orga6f7d59"></a>

### What impedes performance

-   Physical constraints: power consumption, heat dissipation, CPU frequency&#x2026;
    
    It is relevant to programmers.
-   Cost (money)
-   Sharing: tradeoffs in favor of some at cost of someone else
-   Simplicity is often slow


<a id="org44294b3"></a>

### Latency and throughput

-   Decreasing  latency: exploiting workload properties


<a id="org64e1aeb"></a>

### Parallelism and concurrency

-   Pthreads-like parallel execution with synchronization
-   Block-synchronous parallelism
-   Actors


<a id="orga8df5e2"></a>

### GPU architecture


<a id="orga036b92"></a>

### actor model

-   queue and workers
-   Examples: erlang, akka, elixir


<a id="org50060eb"></a>

## Errors


<a id="org2922469"></a>

### Basic steps

-   Error detection
    Requires *redundancy* for more information.
    -   In networks: discarding damaged frames
-   Error containment
    Modules are also designed to fail independently of other modules

-   Error masking
    Either correct errors or give a good enough default
    -   In memory
        -   how hardware memory fails
        -   error correction codes
    
    -   In networks
        -   resend packet on timeout
        -   send packets around failed nodes


<a id="org4f42a40"></a>

### Faults, Failures, Errors

-   Fault is a defect that may cause problems
    Faults come from:
    -   Hardware and software
    -   Design and implementation
    -   Operations and environment
-   Failure is a case when things went wrong
-   Errors are wrong computation results


<a id="org85ffd8a"></a>

# Notes

useful:
<https://nielshagoort.com/2019/03/12/exploring-the-gpu-architecture/#:~:text=Its%20architecture%20is%20tolerant%20of,the%20retrieve%20data%20from%20memory>.

Notes:

-   layers on circuit boards
-   FGPA

